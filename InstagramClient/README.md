### Author: Dan Mayer (2016)
[linkedin.com/in/hawaiidanmayer](http://www.linkedin.com/in/hawaiidanmayer)

# README #

This project demonstrates a simple Android client to utilize the Instagram API to authenticate a user, see posts, see liked media and followed users recent posts.

# InstagramClient

A simple Android Instagram client example.

## Note
Before compiling the app make sure to put your client_id and client_secret on [strings.xml] (app/src/main/res/values/strings.xml).
For more information please visit [Instagram Developer Documentation](https://www.instagram.com/developer).

## License    
See the [LICENSE](LICENSE.md) file for license rights and limitations.

Android�s native java based application architecture is a pleasure to build applications with in part because of it�s easy to understand but limited by state management via SharedPreferences, or  complexity (consider Activity and Fragment lifecycle management). 

Here are some Android techniques I use to deal with state management and nested fragments in this example application which demonstrates a practical reusable architectural patterns in the form of activities and nested fragments using the popular Instagram API for authentication and data, and using SQLite for application state persistence.

## Project overview

[Screencast demo](http://www.screencast.com/t/Vi42qZtfJPk)

The app authenticates the user and caches network request response object graphs ("designed for offline" application state management) within asynchronous task loaders within the managed nested fragment lifecycle. �Designed for Offline� is a phrase heard in recent times for developers to make sure mobile apps work in a network disconnected scenario. While there are any number of ways to address caching network requests and in maintaining current application configuration state in Android I�ll show you the basic idea to solve this using SQLite, with a table for application state and another for network request cache.  

### The StateManager class
I�ve cut out most of the class and want to start by looking at the important methods
/* StateManager used to persist user state to persistent storage using SQLite store
*  Implementation by two tables: configuration tuples and request/response cache.
* */
public class StateManager {�

/* Application configuration  */
public void setCode(String code, Object value, String type){...}
public Object getCode(String code, Object defaultValue){...}
/* Cache object graph derived from network request */
public void setJson(String request, byte[] response){...}

public byte[] getJsonCache(String request){...}

As you can see, the StateManager can store and retrieve codes and requests. I�ll address these separately.

### Storing Codes
The StateManager provides access to a persisted state data tier within a SQLite file on a local drive. There is a debate on the best way to handle global application data, with shared preferences or a dedicated singleton but I prefer to modify the application base class:

/* AppState used to persist user application memory.
   Note alternative implementation in singleton:
   http://stackoverflow.com/questions/3826905/singletons-vs-application-context-in-android
*/
public class AppState extends Application {
    private String accessToken;
    public String getAccessToken() {
        return accessToken;
    }
    public void setAccessToken(String accessToken_) {
        accessToken = accessToken_;
    }
...

To initialize the application configuration from the SQLite file on the local drive: 

StateManager stateManager = new StateManager(getBaseContext());
appState=(AppState)getApplicationContext();
appState.setAccessToken((String)stateManager.getCode(StateManager.Codes.AccessToken,""));

Subsequently, throughout the app we can retrieve the users authentication token:

appState=(AppState)getApplicationContext();
if (appState.getAccessToken() != "") {
    Intent intent = new Intent(this, TabsActivity.class);
    startActivity(intent);
}

### Cached network requests
A typical client application utilizes data from server calls. In the Instagram sample, we have use an asynchronous task loader to call the remote server, wait and process the data, then returning the data to a data adapter for use in views.
byte[] bytes = stateManager.getJsonCache(tokenURLString);
if(bytes!=null && bytes.length>0){
    images=(List<Media>) convertFromBytes(bytes);
Serializing and deserializing the object graph is done as follows:
public static byte[] convertToBytes(Object object) throws IOException {
    try (ByteArrayOutputStream bos = new ByteArrayOutputStream();
         ObjectOutput out = new ObjectOutputStream(bos)) {
        out.writeObject(object);
        return bos.toByteArray();
    }
}
public static Object convertFromBytes(byte[] bytes) throws IOException, ClassNotFoundException {
    try (ByteArrayInputStream bis = new ByteArrayInputStream(bytes);
         ObjectInput in = new ObjectInputStream(bis)) {
        return in.readObject();
    }
}

### Asynchronous task loaders in nested fragments
The samples design pattern includes a folder for interfaces and asynchronous tasks which work together to do background processing and notify the calling thread when complete. Consider the GetMediaAsyncTask doInBackground and onPostExecute methods below which checks the cache before calling the network to get new data and populate the requested object graph, then calling onPostExecute to notify the calling thread.

public class GetMediaAsyncTask extends AsyncTask<String, Void, List<Media>> {
    �
@Override
   protected List<Media> doInBackground(String... params) {
        List<Media> images = new ArrayList<>();
        try {
            URL url = new URL(tokenURLString);
            HttpsURLConnection httpsURLConnection = (HttpsURLConnection) url.openConnection();
            StateManager stateManager = new StateManager(context);
            byte[] bytes = stateManager.getJsonCache(tokenURLString);
            if(bytes!=null && bytes.length>0){
                images=(List<Media>) AndroidUtilities.convertFromBytes(bytes);
            }
            if(images.size()==0) {
                String jsonString = util.getUrlString(httpsURLConnection);
                JSONObject jsonObject = (JSONObject) new JSONTokener(jsonString).nextValue();
                JSONArray jsonArray = jsonObject.getJSONArray("data");
                for (int i = 0; i < jsonArray.length(); i++) {
                    � parsing ommitted for brevity � 
                    images.add(image);
                }                stateManager.setJson(tokenURLString,AndroidUtilities.convertToBytes(images));
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        return images;
    }
    @Override
    protected void onPostExecute(List<Media> images) {
        if (images.size() != 0)
            delegate.getMediaFinish(images);
    }
}



