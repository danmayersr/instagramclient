package com.mayer.instagramclient.interfaces;

import com.mayer.instagramclient.model.Follower;

import java.util.List;

public interface FollowsAsyncResponseInterface {
    void getFollowsFinish(List<Follower> images);
}

