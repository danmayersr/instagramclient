package com.mayer.instagramclient.fragments;

import android.support.v4.app.Fragment;

import com.mayer.instagramclient.model.AppState;

public class FragmentBase extends Fragment {
    protected AppState getAppState(){
        return (AppState)getContext().getApplicationContext();
    }
}
