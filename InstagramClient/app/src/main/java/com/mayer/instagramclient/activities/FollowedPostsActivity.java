package com.mayer.instagramclient.activities;

import android.graphics.Bitmap;
import android.graphics.drawable.Drawable;
import android.os.Bundle;
import android.provider.ContactsContract;
import android.support.design.widget.TabLayout;
import android.support.v4.graphics.drawable.RoundedBitmapDrawable;
import android.support.v4.graphics.drawable.RoundedBitmapDrawableFactory;
import android.support.v4.view.ViewPager;
import android.support.v7.app.AppCompatActivity;
import android.widget.ImageView;
import android.widget.TextView;
import com.mayer.instagramclient.R;
import com.mayer.instagramclient.adapters.ViewPagerAdapter;
import com.mayer.instagramclient.fragments.FollowerPostsGridFragment;
import com.mayer.instagramclient.fragments.FollowerPostsListFragment;
import com.mayer.instagramclient.fragments.PostsGridFragment;
import com.mayer.instagramclient.fragments.PostsListFragment;
import com.mayer.instagramclient.model.Follower;
import com.squareup.picasso.Picasso;
import com.squareup.picasso.Target;

public class FollowedPostsActivity extends AppCompatActivity  {
    public static String TAG_FOLLOWER="Follower";
    private Follower follower;
    private TabsActivity.SectionsPagerAdapter mSectionsPagerAdapter;
    private ViewPager mViewPager;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_followed_posts);
        TextView textView = (TextView) findViewById(R.id.section_label);
        Bundle extras = getIntent().getExtras();
        follower = (Follower)getIntent().getSerializableExtra(TAG_FOLLOWER);
        textView.setText(follower.getUserFullName());
        ViewPager viewPager = (ViewPager) findViewById(R.id.viewpager_posts);
        if(viewPager!=null) {
            ViewPagerAdapter adapter = new ViewPagerAdapter(getSupportFragmentManager());
            adapter.addFragment(FollowerPostsListFragment.newInstance(follower.getId()), "List");
            adapter.addFragment(FollowerPostsGridFragment.newInstance(follower.getId()), "Grid");
            viewPager.setAdapter(adapter);
        }
        TabLayout tabLayout = (TabLayout)findViewById(R.id.tabs_posts);
        if (tabLayout != null) {
            tabLayout.setupWithViewPager(viewPager);
            tabLayout.getTabAt(0).setIcon(R.drawable.ic_tab_list);
            tabLayout.getTabAt(1).setIcon(R.drawable.ic_tab_grid);
        }
        textViewDescription= (TextView) findViewById(R.id.section_label);
        textViewDescription.setText(follower.getUserFullName());
        imageViewProfilePicture=(ImageView)findViewById(R.id.imageViewProfilePicture);
        loadPicture();
    }
    private TextView textViewDescription;
    private ImageView imageViewProfilePicture;
    private void loadPicture(){
        // Load Profile Picture
        Picasso.with(this)
                .load(follower.getUserPicture())
                .into(new Target() {
                    @Override
                    public void onBitmapLoaded(Bitmap bitmap, Picasso.LoadedFrom from) {
                        // Transform profile picture to circle
                        RoundedBitmapDrawable roundedBitmapDrawable = RoundedBitmapDrawableFactory.create(getResources(), bitmap);
                        roundedBitmapDrawable.setCircular(true);
                        imageViewProfilePicture.setImageDrawable(roundedBitmapDrawable);
                    }
                    @Override
                    public void onBitmapFailed(Drawable errorDrawable) {
                    }
                    @Override
                    public void onPrepareLoad(Drawable placeHolderDrawable) {
                    }
                });
    }
}
