package com.mayer.instagramclient.asyncTasks;

import android.content.Context;
import android.os.FileObserver;
import android.support.v4.content.AsyncTaskLoader;

import java.io.File;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by Dan on 7/26/2016.
 */
public class JsonAsyncTaskLoader extends AsyncTaskLoader<List<?>> {
    private List<?> mData;
    private FileObserver mFileObserver;
    public JsonAsyncTaskLoader(Context context) {
        super(context);
    }
    @Override
    protected void onStartLoading() {
        if (mData != null) {// Use cache
            deliverResult(mData);
        }
        if (mFileObserver == null) {
            String path = new File(
                    getContext().getFilesDir(), "downloaded.json").getPath();
            mFileObserver = new FileObserver(path) {
                @Override
                public void onEvent(int event, String path) {
                    // Notify the loader to reload the data
                    onContentChanged();
                    // If the loader is started, this will kick off
                    // loadInBackground() immediately. Otherwise,
                    // the fact that something changed will be cached
                    // and can be later retrieved via takeContentChanged()
                }
            };
            mFileObserver.startWatching();
        }
        if (takeContentChanged() || mData == null) {
            // Something has changed or we have no data
            forceLoad();
        }
    }
    @Override
    public List<?> loadInBackground() {
        // This is on a background thread
        File jsonFile = new File(
                getContext().getFilesDir(), "downloaded.json");
        List<?> data = new ArrayList<>();
        // Parse the JSON using the library of your choice
        return data;
    }
    @Override
    public void deliverResult(List<?> data) {
        // We’ll save the data for later retrieval
        mData = data;
        // We can do any pre-processing we want here
        // Just remember this is on the UI thread so nothing lengthy!
        super.deliverResult(data);
    }
    protected void onReset() {
        // Stop watching for file changes
        if (mFileObserver != null) {
            mFileObserver.stopWatching();
            mFileObserver = null;
        }
    }
}
