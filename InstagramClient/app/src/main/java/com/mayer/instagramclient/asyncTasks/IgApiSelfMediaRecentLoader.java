package com.mayer.instagramclient.asyncTasks;

import android.content.Context;
import android.os.FileObserver;
import android.support.v4.content.AsyncTaskLoader;

import com.mayer.instagramclient.model.Media;
import com.mayer.instagramclient.model.StateManager;

import java.io.File;
import java.util.ArrayList;
import java.util.List;

public class IgApiSelfMediaRecentLoader extends AsyncTaskLoader<List<Media>> {
    private List<Media> mData;
    private Context context;
    public IgApiSelfMediaRecentLoader(Context context_) {
        super(context_);
        context=context_;
    }
    @Override
    protected void onStartLoading() {
        if (mData != null) {// Use cache
            deliverResult(mData);
        }
        StateManager dh = new StateManager(context);
        String json= (String)dh.getCode(StateManager.Codes.SelfMediaRecent,"string");
        if (json == "") {

        }
        if (takeContentChanged() || mData == null) {
            // Something has changed or we have no data
            forceLoad();
        }
    }
    @Override
    public List<Media> loadInBackground() {
        List<Media> data = new ArrayList<>();


        // Parse the JSON using the library of your choice
        return data;
    }
    @Override
    public void deliverResult(List<Media> data) {
        // We’ll save the data for later retrieval
        mData = data;
        // We can do any pre-processing we want here
        // Just remember this is on the UI thread so nothing lengthy!
        super.deliverResult(data);
    }
    protected void onReset() {

    }
}
