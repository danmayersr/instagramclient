package com.mayer.instagramclient.model;

import java.io.Serializable;

public class Follower implements Serializable {
    private String userName;
    private String profile_picture;
    private String full_name;
    private String id;

    public String getUserName() {
        return userName;
    }
    public void setUserName(String userName) {
        this.userName = userName;
    }

    public String getUserPicture() {
        return profile_picture;
    }
    public void setUserPicture(String profile_picture) {
        this.profile_picture = profile_picture;
    }

    public String getUserFullName() {
        return full_name;
    }
    public void setUserNameName(String full_name) {
        this.full_name = full_name;
    }

    public String getId() {
        return id;
    }
    public void setId(String id) {
        this.id = id;
    }

}
