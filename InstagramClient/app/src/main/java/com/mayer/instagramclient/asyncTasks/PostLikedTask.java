package com.mayer.instagramclient.asyncTasks;

import com.mayer.instagramclient.AndroidUtilities;
import java.net.URL;
import javax.net.ssl.HttpsURLConnection;

public class PostLikedTask{
    private final String APIURL = "https://api.instagram.com/v1/media/";
    private AndroidUtilities util = new AndroidUtilities();
    public void post(String mediaId, String accessToken, Boolean like) {
        String tokenURLString = APIURL + mediaId+"/likes?access_token=" + accessToken;
        try {
            URL url = new URL(tokenURLString);
            HttpsURLConnection httpsURLConnection = (HttpsURLConnection) url.openConnection();
            if(like)
                util.postUrl(httpsURLConnection);
            else
                util.deleteUrl(httpsURLConnection);
        }
        catch (Exception me){}
    }
}
