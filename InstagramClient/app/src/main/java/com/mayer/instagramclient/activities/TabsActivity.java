package com.mayer.instagramclient.activities;

import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.drawable.Drawable;
import android.os.Bundle;
import android.support.design.widget.TabLayout;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;
import android.support.v4.graphics.drawable.RoundedBitmapDrawable;
import android.support.v4.graphics.drawable.RoundedBitmapDrawableFactory;
import android.support.v4.view.ViewPager;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.Menu;
import android.view.MenuItem;
import android.widget.ImageView;
import android.widget.TextView;

import com.mayer.instagramclient.R;
import com.mayer.instagramclient.fragments.FollowingFragment;
import com.mayer.instagramclient.fragments.LikedFragment;
import com.mayer.instagramclient.fragments.PostsFragment;
import com.mayer.instagramclient.asyncTasks.GetDescriptionAsyncTask;
import com.mayer.instagramclient.interfaces.DescriptionAsyncResponseInterface;
import com.mayer.instagramclient.model.AppState;
import com.mayer.instagramclient.model.StateManager;
import com.squareup.picasso.Picasso;
import com.squareup.picasso.Target;

public class TabsActivity extends AppCompatActivity implements DescriptionAsyncResponseInterface {
    AppState appState;
    /* The {@link android.support.v4.view.PagerAdapter} provides
     * fragments for each of the sections. */
    private SectionsPagerAdapter mSectionsPagerAdapter;
    /**
     * The {@link ViewPager} that will host the section contents.
     */
    private ViewPager mViewPager;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_tabs);
        appState = (AppState) getApplicationContext();
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        // Create the adapter that will return a fragment for each of the three
        // primary sections of the activity.
        mSectionsPagerAdapter = new SectionsPagerAdapter(getSupportFragmentManager());
        // Set up the ViewPager with the sections adapter.
        mViewPager = (ViewPager) findViewById(R.id.container);
        mViewPager.setAdapter(mSectionsPagerAdapter);
        TabLayout tabLayout = (TabLayout) findViewById(R.id.tabs);
        tabLayout.setupWithViewPager(mViewPager);
        //region inflate Profile Info
        TextView textViewName = (TextView) findViewById(R.id.textViewName);
        textViewDescription = (TextView)findViewById(R.id.textViewDescription);
        imageViewProfilePicture = (ImageView)findViewById(R.id.imageViewProfilePicture);
        if (textViewName != null) {
            textViewName.setText(appState.getName());
        }
        if (textViewDescription != null) {
            StateManager dh = new StateManager(getBaseContext());
            appState.setDescription( (String)dh.getCode(StateManager.Codes.Description,""));
            dh.close();
            dh = null;
            textViewDescription.setText(appState.getDescription());
            //load updated description
            getDescriptionAsyncTask= new GetDescriptionAsyncTask(appState.getAccessToken());
            getDescriptionAsyncTask.delegate = this;
            getDescriptionAsyncTask.execute();
        }
        loadPicture();
    }
    private GetDescriptionAsyncTask getDescriptionAsyncTask;
    private TextView textViewDescription;
    private ImageView imageViewProfilePicture;
    private void loadPicture(){
        // Load Profile Picture
        Picasso.with(this)
                .load(appState.getProfilePicture())
                .into(new Target() {
                    @Override
                    public void onBitmapLoaded(Bitmap bitmap, Picasso.LoadedFrom from) {
                        // Transform profile picture to circle
                        RoundedBitmapDrawable roundedBitmapDrawable = RoundedBitmapDrawableFactory.create(getResources(), bitmap);
                        roundedBitmapDrawable.setCircular(true);
                        imageViewProfilePicture.setImageDrawable(roundedBitmapDrawable);
                    }
                    @Override
                    public void onBitmapFailed(Drawable errorDrawable) {
                    }
                    @Override
                    public void onPrepareLoad(Drawable placeHolderDrawable) {
                    }
                });
    }
    @Override
    public void getDescriptionFinish(String description) {
        textViewDescription.setText(description);
        final AppState appState = (AppState) getApplicationContext();
        appState.setDescription(description);
        StateManager stateManager = new StateManager(getBaseContext());
        stateManager.setCode(StateManager.Codes.Description,description,"string");
        stateManager.close();
        stateManager = null;
    }
    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.menu_tabs, menu);
        return true;
    }
    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int id = item.getItemId();
        if (id == R.id.action_logout) {
            logout();
            return true;
        }
        return super.onOptionsItemSelected(item);
    }
    private void logout(){
        final AppState appState = (AppState) getApplicationContext();
        appState.setAccessToken("");
        appState.setId("");
        appState.setName("");
        appState.setUsername("");
        appState.setProfilePicture("");
        StateManager stateManager = new StateManager(getBaseContext());
        stateManager.reset();
        stateManager.close();
        stateManager = null;
        this.finish();
        Intent intent = new Intent(getApplicationContext(), MainActivity.class);
        startActivity(intent);
    }
    public class SectionsPagerAdapter extends FragmentPagerAdapter {
        public SectionsPagerAdapter(FragmentManager fm) {
            super(fm);
        }
        @Override
        public Fragment getItem(int position) {
            if(position==0) {
                return PostsFragment.newInstance();
            }
            else  if(position==1) {
                return LikedFragment.newInstance();
            } else
                return FollowingFragment.newInstance();
        }
        @Override
        public int getCount() {
            return 3;
        }
        @Override
        public CharSequence getPageTitle(int position) {
            switch (position) {
                case 0:
                    return "My Posts";
                case 1:
                    return "My Likes";
                case 2:
                    return "Following";
            }
            return null;
        }
    }
}
