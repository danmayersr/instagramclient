package com.mayer.instagramclient.asyncTasks;

import android.content.Context;
import android.os.AsyncTask;

import com.mayer.instagramclient.AndroidUtilities;
import com.mayer.instagramclient.model.Media;
import com.mayer.instagramclient.interfaces.MediaAsyncResponseInterface;
import com.mayer.instagramclient.model.StateManager;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;
import org.json.JSONTokener;
import java.net.URL;
import java.util.ArrayList;
import java.util.List;

import javax.net.ssl.HttpsURLConnection;

/**
 * Get user's media
 */
public class GetFollowsMediaAsyncTask extends AsyncTask<Void, Void, List<Media>> {
    public GetFollowsMediaAsyncTask(String accessToken_, Context context_, MediaAsyncResponseInterface delegate_ ){accessToken=accessToken_;context=context_; delegate=delegate_;}
    private Context context;
    private String accessToken;
    private final String APIURL_FOLLOWS = "https://api.instagram.com/v1/users/self/follows/";
    private AndroidUtilities util = new AndroidUtilities();
    public MediaAsyncResponseInterface delegate = null;
    @Override
    protected List<Media> doInBackground(Void... voids) {
        String tokenURLString = APIURL_FOLLOWS + "?access_token=" + accessToken;
        List<Media> images = new ArrayList<>();
        try {
            URL url = new URL(tokenURLString);
            HttpsURLConnection httpsURLConnection = (HttpsURLConnection) url.openConnection();
            StateManager stateManager = new StateManager(context);
            byte[] bytes = stateManager.getJsonCache(tokenURLString);
            if(bytes!=null && bytes.length>0){
                images=(List<Media>) AndroidUtilities.convertFromBytes(bytes);
            }
            else {
                String jsonString = util.getUrlString(httpsURLConnection);
                JSONObject jsonObject = (JSONObject) new JSONTokener(jsonString).nextValue();
                JSONArray jsonArray = jsonObject.getJSONArray("data");
                for (int i = 0; i < jsonArray.length(); i++) {
                    String caption = null;
                    String time = jsonArray.getJSONObject(i).getString("created_time");
                    String imageURL = jsonArray.getJSONObject(i).getJSONObject("images").getJSONObject("standard_resolution").getString("url");
                    String mediaURL = jsonArray.getJSONObject(i).getString("link");
                    try { // Handle null caption!
                        caption = jsonArray.getJSONObject(i).getJSONObject("caption").getString("text");
                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
                    Media image = new Media();
                    image.setTime(time);
                    image.setImageURL(imageURL);
                    image.setMediaURL(mediaURL);
                    image.setCaption(caption);
                    images.add(image);
                }
                stateManager.setJson(tokenURLString,AndroidUtilities.convertToBytes(images));
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        return images;
    }
    @Override
    protected void onPostExecute(List<Media> images) {
        if (images.size() != 0)
            delegate.getMediaFinish(images);
    }
}