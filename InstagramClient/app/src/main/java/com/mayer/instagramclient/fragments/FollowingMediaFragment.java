package com.mayer.instagramclient.fragments;

import android.content.ActivityNotFoundException;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;
import com.mayer.instagramclient.R;
import com.mayer.instagramclient.adapters.GridAdapter;
import com.mayer.instagramclient.asyncTasks.GetSelfMediaAsyncTask;
import com.mayer.instagramclient.interfaces.ClickListener;
import com.mayer.instagramclient.interfaces.MediaAsyncResponseInterface;
import com.mayer.instagramclient.model.Media;
import com.mayer.instagramclient.activities.RecyclerTouchListener;
import java.util.ArrayList;
import java.util.List;

public class FollowingMediaFragment extends FragmentBase implements MediaAsyncResponseInterface {
    private GetSelfMediaAsyncTask getSelfMediaAsyncTask;
    private RecyclerView recyclerView;
    private GridAdapter gridAdapter;
    private List<Media> medias = new ArrayList<Media>();
    public static FollowingMediaFragment newInstance() {
        return new FollowingMediaFragment();
    }

    @Override
    public View onCreateView(LayoutInflater inflater,
                             ViewGroup container,
                             Bundle savedInstanceState) {
        View rootView = inflater.inflate(R.layout.fragment_following, container, false);
        gridAdapter = new GridAdapter(getActivity(), medias);
        TextView textView = (TextView) rootView.findViewById(R.id.section_label);
        textView.setText("Following");
        recyclerView = (RecyclerView) rootView.findViewById(R.id.recyclerViewImages);
        RecyclerView.LayoutManager mLayoutManager = new GridLayoutManager(getActivity().getApplicationContext(), 3);
        if (recyclerView != null) {
            recyclerView.setLayoutManager(mLayoutManager);
            recyclerView.setHasFixedSize(true);
            recyclerView.setAdapter(gridAdapter);
            recyclerView.addOnItemTouchListener(new RecyclerTouchListener(getActivity().getApplicationContext(), recyclerView, new ClickListener() {
                @Override
                public void onClick(View view, int position) {
                    Media m = medias.get(position);
                    String videoURL = m.getMediaURL();
                    if (videoURL != null){
                        Uri uri = Uri.parse(m.getImageURL());
                        Intent intent = new Intent(Intent.ACTION_VIEW, uri);
                        intent.setPackage("com.instagram.android");
                        try {
                            startActivity(intent);
                        } catch (ActivityNotFoundException e) {
                            startActivity(new Intent(Intent.ACTION_VIEW,
                                    Uri.parse(videoURL)));
                        }
                    }
                }
                @Override
                public void onLongClick(View view, int position) {
                }
            }));
        }
        return rootView;
    }
    @Override
    public void onStart() {
        super.onStart();
        getSelfMediaAsyncTask = new GetSelfMediaAsyncTask(getAppState().getAccessToken(), getContext(), this);
        getSelfMediaAsyncTask.execute();
    }
   @Override
    public void getMediaFinish(List<Media> media) {
        for (Media m : media) {
            this.medias.add(m);
        }
        gridAdapter.notifyDataSetChanged();
    }
}

