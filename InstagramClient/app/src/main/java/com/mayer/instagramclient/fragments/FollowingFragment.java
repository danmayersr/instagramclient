package com.mayer.instagramclient.fragments;

import android.content.ActivityNotFoundException;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import com.mayer.instagramclient.activities.FollowedPostsActivity;
import com.mayer.instagramclient.interfaces.ClickListener;
import com.mayer.instagramclient.R;
import com.mayer.instagramclient.adapters.FollowerGridAdapter;
import com.mayer.instagramclient.asyncTasks.GetFollowsAsyncTask;
import com.mayer.instagramclient.interfaces.FollowsAsyncResponseInterface;
import com.mayer.instagramclient.model.Follower;
import com.mayer.instagramclient.activities.RecyclerTouchListener;

import java.util.ArrayList;
import java.util.List;

public class FollowingFragment extends FragmentBase implements FollowsAsyncResponseInterface {
    private GetFollowsAsyncTask getFollowsAsyncTask;
    private RecyclerView recyclerView;
    private FollowerGridAdapter gridAdapter;
    private List<Follower> follows = new ArrayList<Follower>();
    public static FollowingFragment newInstance() {
        return new FollowingFragment();
    }
    @Override
    public View onCreateView(LayoutInflater inflater,
                             ViewGroup container,
                             Bundle savedInstanceState) {
        View rootView = inflater.inflate(R.layout.fragment_following, container, false);
        gridAdapter = new FollowerGridAdapter(getActivity(), follows);
        recyclerView = (RecyclerView) rootView.findViewById(R.id.recyclerViewImages);
        LinearLayoutManager mLayoutManager = new LinearLayoutManager(getContext());
        if (recyclerView != null) {
            recyclerView.setLayoutManager(mLayoutManager);
            recyclerView.setHasFixedSize(true);
            recyclerView.setAdapter(gridAdapter);
            recyclerView.addOnItemTouchListener(new RecyclerTouchListener(getContext(), recyclerView, new ClickListener() {
                @Override
                public void onClick(View view, int position) {
                    Follower follower = follows.get(position);
                    Intent intent = new Intent(getContext(), FollowedPostsActivity.class);
                    intent.putExtra(FollowedPostsActivity.TAG_FOLLOWER, follower);
                    startActivity(intent);
                }
                @Override
                public void onLongClick(View view, int position) {
                    Follower follower = follows.get(position);
                    String videoURL = follower.getUserPicture();
                    if (videoURL != null){
                        Uri uri = Uri.parse(follower.getUserPicture());
                        Intent intent = new Intent(Intent.ACTION_VIEW, uri);
                        intent.setPackage("com.instagram.android");
                        try {
                            startActivity(intent);
                        } catch (ActivityNotFoundException e) {
                            startActivity(new Intent(Intent.ACTION_VIEW,
                                    Uri.parse(follower.getUserPicture())));
                        }
                    }
                }
            }));
        }
        return rootView;
    }
    @Override
    public void onStart() {
        super.onStart();
        if (getFollowsAsyncTask == null) {
            getFollowsAsyncTask = new GetFollowsAsyncTask(getAppState().getAccessToken(), getContext(), this);
            getFollowsAsyncTask.execute();
        }
    }
    @Override
    public void getFollowsFinish(List<Follower> followers) {
        for (Follower follower : followers) {
            this.follows.add(follower);
        }
        gridAdapter.notifyDataSetChanged();
    }
}

