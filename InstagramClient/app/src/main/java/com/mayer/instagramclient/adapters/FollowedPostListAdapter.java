package com.mayer.instagramclient.adapters;

import android.content.Context;
import android.graphics.Bitmap;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.mayer.instagramclient.R;
import com.mayer.instagramclient.asyncTasks.PostLikedTask;
import com.mayer.instagramclient.model.AppState;
import com.mayer.instagramclient.model.Media;
import com.squareup.picasso.Picasso;
import com.squareup.picasso.Transformation;
import java.util.List;

public class FollowedPostListAdapter extends RecyclerView.Adapter<FollowedPostListAdapter.FollowedPostViewHolder> {
    private List<Media> mediaList;
    private Context context;
    public FollowedPostListAdapter(Context context, List<Media> mediaList) {
        this.mediaList = mediaList;
        this.context = context;
    }
    @Override
    public FollowedPostViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.list_layout_likable_images, parent, false);
        return new FollowedPostViewHolder(itemView);
    }
    @Override
    public void onBindViewHolder(final FollowedPostViewHolder holder, int position) {
        Media image = mediaList.get(position);
        /* Resize images to fit screen */
        Transformation transformation = new Transformation() {
            @Override
            public Bitmap transform(Bitmap source) {
                int targetWidth = holder.imageViewImage.getWidth();
                double aspectRatio = (double) source.getHeight() / (double) source.getWidth();
                int targetHeight = (int) (targetWidth * aspectRatio);
                Bitmap result = Bitmap.createScaledBitmap(source, targetWidth, targetHeight, false);
                if (result != source) {
                    // Same bitmap is returned if sizes are the same
                    source.recycle();
                }
                return result;
            }
            @Override
            public String key() {
                return "transformation" + " desiredWidth";
            }
        };
        holder.textViewDate.setText(image.getDate());
        Picasso.with(context).load(image.getImageURL()).transform(transformation).into(holder.imageViewImage); // Loading image using Picasso library
        holder.textViewCaption.setText(image.getCaption());
    }
    @Override
    public int getItemCount() {
        return mediaList.size();
    }
    public class FollowedPostViewHolder extends RecyclerView.ViewHolder {
        public TextView textViewDate;
        public ImageView imageViewImage;
        public TextView textViewCaption;
        public Button buttonLike;
        public Button buttonUnlike;
        public FollowedPostViewHolder(View view) {
            super(view);
            textViewDate = (TextView) view.findViewById(R.id.textViewDate);
            textViewCaption = (TextView) view.findViewById(R.id.textViewCaption);
            imageViewImage = (ImageView) view.findViewById(R.id.imageViewImage);
            buttonLike = (Button) view.findViewById(R.id.buttonLike);
            buttonUnlike = (Button) view.findViewById(R.id.buttonUnLike);
            buttonLike.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    final AppState appState = (AppState)view.getContext().getApplicationContext();
                    PostLikedTask postLikedTask=new PostLikedTask();
                    String mediaId=String.valueOf(view.getId());
                    postLikedTask.post(mediaId,appState.getAccessToken(), true);
                    Toast.makeText(view.getContext(), "Liked!", Toast.LENGTH_SHORT).show();
                }
            });
            buttonUnlike.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    final AppState appState = (AppState)view.getContext().getApplicationContext();
                    PostLikedTask postLikedTask=new PostLikedTask();
                    String mediaId=String.valueOf(view.getId());
                    postLikedTask.post(mediaId,appState.getAccessToken(), false);
                    Toast.makeText(view.getContext(), "Unliked :(", Toast.LENGTH_SHORT).show();
                }
            });
        }
    }
}
