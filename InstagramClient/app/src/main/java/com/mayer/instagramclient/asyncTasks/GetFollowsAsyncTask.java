package com.mayer.instagramclient.asyncTasks;

import android.content.Context;
import android.os.AsyncTask;

import com.mayer.instagramclient.AndroidUtilities;
import com.mayer.instagramclient.interfaces.MediaAsyncResponseInterface;
import com.mayer.instagramclient.model.Media;
import com.mayer.instagramclient.interfaces.FollowsAsyncResponseInterface;
import com.mayer.instagramclient.model.Follower;
import com.mayer.instagramclient.model.StateManager;

import org.json.JSONArray;
import org.json.JSONObject;
import org.json.JSONTokener;
import java.net.URL;
import java.util.ArrayList;
import java.util.List;
import javax.net.ssl.HttpsURLConnection;

/**
 * Get user's follows user list
 */
public class GetFollowsAsyncTask extends AsyncTask<Void, Void, List<Follower>> {
    public GetFollowsAsyncTask(String accessToken_, Context context_, FollowsAsyncResponseInterface delegate_){accessToken = accessToken_;context=context_; delegate=delegate_;}
    private String accessToken;
    private Context context;
    private final String APIURL_FOLLOWS = "https://api.instagram.com/v1/users/self/follows";
    private AndroidUtilities util = new AndroidUtilities();
    public FollowsAsyncResponseInterface delegate = null;
    @Override
    protected List<Follower> doInBackground(Void... voids) {
        String tokenURLString = APIURL_FOLLOWS + "?access_token=" + accessToken;
        List<Follower> followers = new ArrayList<>();
        try {
            URL url = new URL(tokenURLString);
            HttpsURLConnection httpsURLConnection = (HttpsURLConnection) url.openConnection();
            StateManager stateManager = new StateManager(context);
            byte[] bytes = stateManager.getJsonCache(tokenURLString);
            if(bytes!=null && bytes.length>0){
                followers=(List<Follower>) AndroidUtilities.convertFromBytes(bytes);
            }
            if(followers.size()==0) {
                String jsonString = util.getUrlString(httpsURLConnection);
                JSONObject jsonObject = (JSONObject) new JSONTokener(jsonString).nextValue();
                JSONArray jsonArray = jsonObject.getJSONArray("data");
                for (int i = 0; i < jsonArray.length(); i++) {
                    Follower follower = new Follower();
                    follower.setId(jsonArray.getJSONObject(i).getString("id"));
                    follower.setUserName(jsonArray.getJSONObject(i).getString("username"));
                    follower.setUserPicture(jsonArray.getJSONObject(i).getString("profile_picture"));
                    follower.setUserNameName(jsonArray.getJSONObject(i).getString("full_name"));
                    followers.add(follower);
                }
                stateManager.setJson(tokenURLString,AndroidUtilities.convertToBytes(followers));
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        return followers;
    }

    @Override
    protected void onPostExecute(List<Follower> followers) {
        if (followers.size() > 0){
            delegate.getFollowsFinish(followers);
        }
    }
}