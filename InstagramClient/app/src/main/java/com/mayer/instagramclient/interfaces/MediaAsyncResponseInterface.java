package com.mayer.instagramclient.interfaces;

import java.util.List;

import com.mayer.instagramclient.model.Media;

/**
 * an interface for exchange list of media.
 */
public interface MediaAsyncResponseInterface {
    void getMediaFinish(List<Media> images);
}
