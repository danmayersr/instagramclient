package com.mayer.instagramclient.fragments;

import android.os.Bundle;
import android.support.design.widget.TabLayout;
import android.support.v4.app.Fragment;
import android.support.v4.view.ViewPager;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.mayer.instagramclient.R;
import com.mayer.instagramclient.adapters.ViewPagerAdapter;

public class FollowerPostsFragment extends Fragment {
    public static String TAG_FOLLOWER_ID="Id";
    public static FollowerPostsFragment newInstance(String id) {
        FollowerPostsFragment followerPostsFragment = new FollowerPostsFragment();
        Bundle args = new Bundle();
        args.putString(TAG_FOLLOWER_ID, id);
        followerPostsFragment.setArguments(args);
        return followerPostsFragment;
    }
    private String id;
   @Override
    public View onCreateView(LayoutInflater inflater,
                             ViewGroup container,
                             Bundle savedInstanceState) {
        View rootView = inflater.inflate(R.layout.fragment_posts, container, false);
        TextView textView = (TextView) rootView.findViewById(R.id.section_label);
        textView.setText("Following Posts");
        ViewPager viewPager = (ViewPager) rootView.findViewById(R.id.viewpager_posts);
        if(viewPager!=null)
        {
            ViewPagerAdapter adapter = new ViewPagerAdapter(this.getActivity().getSupportFragmentManager());
            id=savedInstanceState.getString(TAG_FOLLOWER_ID);
            adapter.addFragment(FollowerPostsListFragment.newInstance(id), "List");
            adapter.addFragment(FollowerPostsGridFragment.newInstance(id), "Grid");
            viewPager.setAdapter(adapter);
        }
        TabLayout tabLayout = (TabLayout)rootView.findViewById(R.id.tabs_posts);
        if (tabLayout != null) {
            tabLayout.setupWithViewPager(viewPager);
            tabLayout.getTabAt(0).setIcon(R.drawable.ic_tab_list);
            tabLayout.getTabAt(1).setIcon(R.drawable.ic_tab_grid);
        }
        return rootView;
    }
}