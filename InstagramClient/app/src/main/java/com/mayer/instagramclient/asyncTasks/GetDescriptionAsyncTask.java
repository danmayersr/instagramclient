package com.mayer.instagramclient.asyncTasks;

import android.os.AsyncTask;
import org.json.JSONObject;
import org.json.JSONTokener;
import java.net.URL;
import javax.net.ssl.HttpsURLConnection;
import com.mayer.instagramclient.AndroidUtilities;
import com.mayer.instagramclient.interfaces.DescriptionAsyncResponseInterface;

/**
 * Get profile description
 */
public class GetDescriptionAsyncTask extends AsyncTask<Void, Void, String> {
    public GetDescriptionAsyncTask(String accessToken_){
        accessToken=accessToken_;
    }
    String accessToken;
    private final String APIURL = "https://api.instagram.com/v1/users/self/";
    private AndroidUtilities util = new AndroidUtilities();
    // Using interface to refresh UI on result.
    public DescriptionAsyncResponseInterface delegate = null;
    @Override
    protected String doInBackground(Void... voids) {
        String description = null;
        String tokenURLString = APIURL + "?access_token=" + accessToken;
        try {
            /* Setup connection */
            URL url = new URL(tokenURLString);
            HttpsURLConnection httpsURLConnection = (HttpsURLConnection) url.openConnection();
            /* Get Response */
            String jsonString = util.getUrlString(httpsURLConnection);
            JSONObject jsonObject = (JSONObject) new JSONTokener(jsonString).nextValue();
            description = jsonObject.getJSONObject("data").getString("bio");
        } catch (Exception e) {
            e.printStackTrace();
        }
        return description;
    }
    @Override
    protected void onPostExecute(String description) {
        if (description != null)
            delegate.getDescriptionFinish(description);
    }
}