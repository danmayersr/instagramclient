package com.mayer.instagramclient.activities;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.View;

import com.mayer.instagramclient.R;
import com.mayer.instagramclient.model.AppState;
import com.mayer.instagramclient.model.StateManager;

public class MainActivity extends AppCompatActivity {
    AppState appState;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        /* initialize state from previous session if available */
        appState=(AppState)getApplicationContext();
        StateManager stateManager = new StateManager(getBaseContext());
        appState.setAccessToken((String)stateManager.getCode(StateManager.Codes.AccessToken,""));
        appState.setId((String)stateManager.getCode(StateManager.Codes.Id,""));
        appState.setUsername((String)stateManager.getCode(StateManager.Codes.UserName,""));
        appState.setName((String)stateManager.getCode(StateManager.Codes.Name,""));
        appState.setProfilePicture((String)stateManager.getCode(StateManager.Codes.ProfilePicture,""));
        stateManager.close();
        stateManager = null;
        /* Show Tabs */
        if (appState.getAccessToken() != "") {
            Intent intent = new Intent(this, TabsActivity.class);
            startActivity(intent);
        }
    }
    @Override
    protected void onRestart() {
        super.onRestart();
        finish();
    }
    /* Request Access Token if it didn't exist */
    public void openLogInActivity(View view) {
        Intent intent = new Intent(getApplicationContext(), LogInActivity.class);
        startActivity(intent);
    }
}
