package com.mayer.instagramclient.fragments;

import android.content.ActivityNotFoundException;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import com.mayer.instagramclient.R;
import com.mayer.instagramclient.adapters.GridAdapter;
import com.mayer.instagramclient.asyncTasks.GetMediaAsyncTask;
import com.mayer.instagramclient.interfaces.ClickListener;
import com.mayer.instagramclient.interfaces.MediaAsyncResponseInterface;
import com.mayer.instagramclient.model.Media;
import com.mayer.instagramclient.activities.RecyclerTouchListener;
import java.util.ArrayList;
import java.util.List;

public class FollowerPostsGridFragment extends FragmentBase implements MediaAsyncResponseInterface {
    public FollowerPostsGridFragment() {}
    public static String TAG_FOLLOWER_ID="Id";
    public static FollowerPostsGridFragment newInstance(String id) {
        FollowerPostsGridFragment _FollowerPostsGridFragment = new FollowerPostsGridFragment();
        Bundle args = new Bundle();
        args.putString(TAG_FOLLOWER_ID, id);
        _FollowerPostsGridFragment.setArguments(args);
        return _FollowerPostsGridFragment;
    }
    private RecyclerView recyclerView;
    private GridAdapter gridAdapter;
    private List<Media> mediaList = new ArrayList<Media>();
    private GetMediaAsyncTask getMediaAsyncTask;
    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }
    @Override
    public void onStart() {
        super.onStart();
        String id=getArguments().getString(TAG_FOLLOWER_ID);
        getMediaAsyncTask=new GetMediaAsyncTask(getAppState().getAccessToken(), getContext(), this);
        getMediaAsyncTask.execute(id);
    }
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View rootView = inflater.inflate(R.layout.fragment_grid, container, false);
        gridAdapter = new GridAdapter(getActivity(), mediaList);
        recyclerView = (RecyclerView) rootView.findViewById(R.id.recyclerViewImages);
        RecyclerView.LayoutManager mLayoutManager = new GridLayoutManager(getContext(), 3);
        if (recyclerView != null) {
            recyclerView.setLayoutManager(mLayoutManager);
            recyclerView.setHasFixedSize(true);
            recyclerView.setAdapter(gridAdapter);
            recyclerView.addOnItemTouchListener(new RecyclerTouchListener(getContext(), recyclerView, new ClickListener() {
                @Override
                public void onClick(View view, int position) {
                    Media media = mediaList.get(position);
                    String videoURL = media.getImageURL();
                    if (videoURL != null){
                        Uri uri = Uri.parse(media.getImageURL());
                        Intent intent = new Intent(Intent.ACTION_VIEW, uri);
                        intent.setPackage("com.instagram.android");
                        try {
                            startActivity(intent);
                        } catch (ActivityNotFoundException e) {
                            startActivity(new Intent(Intent.ACTION_VIEW,
                                    Uri.parse(media.getImageURL())));
                        }
                    }
                }
                @Override
                public void onLongClick(View view, int position) {
                }
            }));
        }
        return rootView;
    }
    @Override
    public void getMediaFinish(List<Media> medias) {
        for (Media media : medias) {
            mediaList.add(media);
        }
        gridAdapter.notifyDataSetChanged();
    }
}
