package com.mayer.instagramclient.model;

import android.app.Application;

/* AppState used to persist user application memory.
   Note alternative implementation in singleton:
   http://stackoverflow.com/questions/3826905/singletons-vs-application-context-in-android
*/
public class AppState extends Application {
    private String accessToken;
    public String getAccessToken() {
        return accessToken;
    }
    public void setAccessToken(String accessToken_) {
        accessToken = accessToken_;
    }

    private String username;
    public String getUsername() {
        return username;
    }
    public void setUsername(String username_) {
        username = username_;
    }

    private String name;
    public String getName() {
        return name;
    }
    public void setName(String name_) {
        name = name_;
    }

    private String id;
    public String getId() {
        return id;
    }
    public void setId(String id_) {
        id = id_;
    }

    private String profilePicture;
    public String getProfilePicture() {
        return profilePicture;
    }
    public void setProfilePicture(String profilePicture_) {
        profilePicture = profilePicture_;
    }

    private String description;
    public String getDescription() {
        return description;
    }
    public void setDescription(String description_) {
        description = description_;
    }

}

