package com.mayer.instagramclient.interfaces;

/**
 * an interface for exchange Description.
 */
public interface DescriptionAsyncResponseInterface {
    void getDescriptionFinish(String description);
}
