package com.mayer.instagramclient.fragments;

import android.os.Bundle;
import android.support.design.widget.TabLayout;
import android.support.v4.app.Fragment;
import android.support.v4.view.ViewPager;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.mayer.instagramclient.R;

import com.mayer.instagramclient.adapters.ViewPagerAdapter;

public class PostsFragment extends Fragment {
    public static PostsFragment newInstance() {
        return new PostsFragment();
    }
    @Override
    public View onCreateView(LayoutInflater inflater,
                             ViewGroup container,
                             Bundle savedInstanceState) {
        View rootView = inflater.inflate(R.layout.fragment_posts, container, false);
        TextView textView = (TextView) rootView.findViewById(R.id.section_label);
        textView.setText(R.string.tab_subtitle_0);
        ViewPager viewPager = (ViewPager) rootView.findViewById(R.id.viewpager_posts);
        if(viewPager!=null)
            setupViewPager(viewPager);
        TabLayout tabLayout = (TabLayout)rootView.findViewById(R.id.tabs_posts);
        if (tabLayout != null) {
            tabLayout.setupWithViewPager(viewPager);
            tabLayout.getTabAt(0).setIcon(R.drawable.ic_tab_list);
            tabLayout.getTabAt(1).setIcon(R.drawable.ic_tab_grid);
        }
        return rootView;
    }
    private void setupViewPager(ViewPager viewPager) {
        ViewPagerAdapter adapter = new ViewPagerAdapter(this.getActivity().getSupportFragmentManager());
        adapter.addFragment(new PostsListFragment(), "List");
        adapter.addFragment(new PostsGridFragment(), "Grid");
        viewPager.setAdapter(adapter);
    }
}
