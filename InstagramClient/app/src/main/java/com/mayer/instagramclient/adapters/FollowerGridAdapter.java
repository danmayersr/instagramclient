package com.mayer.instagramclient.adapters;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.mayer.instagramclient.model.Follower;
import com.mayer.instagramclient.R;
import com.squareup.picasso.Picasso;
import java.util.List;

public class FollowerGridAdapter extends RecyclerView.Adapter<FollowerGridAdapter.FollowerViewHolder> {
    private List<Follower> followers;
    private Context context;
    public FollowerGridAdapter(Context context, List<Follower> followers) {
        this.followers = followers;
        this.context = context;
    }
    @Override
    public FollowerViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.fragment_followed_user, parent, false);
        return new FollowerViewHolder(itemView);
    }
    @Override
    public void onBindViewHolder(final FollowerViewHolder holder, int position) {
        Follower follower = followers.get(position);
        holder.textView.setText(follower.getUserName());
        Picasso.with(context).load(follower.getUserPicture()).into(holder.imageViewImage); // Loading image using Picasso library
    }
    @Override
    public int getItemCount() {
        return followers.size();
    }
    public class FollowerViewHolder extends RecyclerView.ViewHolder {
        public TextView textView;
        public ImageView imageViewImage;
        public FollowerViewHolder(View view) {
            super(view);
            imageViewImage = (ImageView) view.findViewById(R.id.icon);
            textView=(TextView)view.findViewById(R.id.label);
        }
    }
}

