package com.mayer.instagramclient.fragments;

import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import java.util.ArrayList;
import java.util.List;
import com.mayer.instagramclient.adapters.ListAdapter;
import com.mayer.instagramclient.asyncTasks.GetSelfMediaAsyncTask;
import com.mayer.instagramclient.interfaces.MediaAsyncResponseInterface;
import com.mayer.instagramclient.model.Media;
import com.mayer.instagramclient.activities.DividerItemDecoration;
import com.mayer.instagramclient.activities.EndlessRecyclerOnScrollListener;
import com.mayer.instagramclient.R;

public class PostsListFragment extends FragmentBase  implements MediaAsyncResponseInterface {
    private RecyclerView recyclerView;
    private ListAdapter listAdapter;
    private List<Media> mediaList = new ArrayList<Media>();
    private static String maxID = null; // Last image id in list
    private boolean loaded = false; // true when get all images
    GetSelfMediaAsyncTask getSelfMediaAsyncTask;
    public PostsListFragment() {}
    public void getMediaFinish(List<Media> medias) {
        for (Media media : medias) {
            mediaList.add(media);
        }
        listAdapter.notifyDataSetChanged();
    }
    @Override
    public void onStart() {
        super.onStart();
        if (getSelfMediaAsyncTask == null && mediaList.size()==0) {
            getSelfMediaAsyncTask = new GetSelfMediaAsyncTask(getAppState().getAccessToken(), getContext(), this);
            getSelfMediaAsyncTask.execute();
        }
    }
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View rootView = inflater.inflate(R.layout.fragment_list, container, false);
        listAdapter = new ListAdapter(getActivity(), mediaList);
        recyclerView = (RecyclerView) rootView.findViewById(R.id.recyclerViewImages);
        LinearLayoutManager linearLayoutManager = new LinearLayoutManager(getContext());
        if (recyclerView != null) {
            recyclerView.setLayoutManager(linearLayoutManager);
            recyclerView.addOnScrollListener(new EndlessRecyclerOnScrollListener(linearLayoutManager) {
                @Override
                public void onLoadMore(int current_page) {
                //    if (!loaded)
                //        new GetSelfMediaAsyncTask(getAppState().getAccessToken(), getContext(), this).execute();
                }
            });
            recyclerView.setHasFixedSize(true);
            recyclerView.addItemDecoration(new DividerItemDecoration(getContext(), LinearLayoutManager.VERTICAL));
            recyclerView.setAdapter(listAdapter);
        }
        return rootView;
    }
    @Override
    public void onStop() {
        super.onStop();
        recyclerView.clearOnScrollListeners();
    }
}
