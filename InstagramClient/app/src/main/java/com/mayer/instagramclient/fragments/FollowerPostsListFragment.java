package com.mayer.instagramclient.fragments;

import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import com.mayer.instagramclient.R;
import com.mayer.instagramclient.adapters.FollowedPostListAdapter;
import com.mayer.instagramclient.adapters.ListAdapter;
import com.mayer.instagramclient.asyncTasks.GetMediaAsyncTask;
import com.mayer.instagramclient.interfaces.MediaAsyncResponseInterface;
import com.mayer.instagramclient.model.Media;
import com.mayer.instagramclient.activities.DividerItemDecoration;
import com.mayer.instagramclient.activities.EndlessRecyclerOnScrollListener;
import java.util.ArrayList;
import java.util.List;

public class FollowerPostsListFragment extends  FragmentBase implements MediaAsyncResponseInterface {
    public FollowerPostsListFragment() {}
    public static String TAG_FOLLOWER_ID="Id";
    public static FollowerPostsListFragment newInstance(String id) {
        FollowerPostsListFragment _FollowerPostsListFragment = new FollowerPostsListFragment();
        Bundle args = new Bundle();
        args.putString(TAG_FOLLOWER_ID, id);
        _FollowerPostsListFragment.setArguments(args);
        return _FollowerPostsListFragment;
    }
    private RecyclerView recyclerView;
    private FollowedPostListAdapter listAdapter;
    private List<Media> mediaList = new ArrayList<Media>();
    private static String maxID = null; // Last image id in list
    GetMediaAsyncTask getMediaAsyncTask;
    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }
    @Override
    public void onStart() {
        super.onStart();
        maxID = null;
        String id=getArguments().getString(TAG_FOLLOWER_ID);
        getMediaAsyncTask= new GetMediaAsyncTask(getAppState().getAccessToken(), getContext(), this);
        getMediaAsyncTask.execute(id);
    }
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View rootView = inflater.inflate(R.layout.fragment_list, container, false);
        listAdapter = new FollowedPostListAdapter(getActivity(), mediaList);
        recyclerView = (RecyclerView) rootView.findViewById(R.id.recyclerViewImages);
        LinearLayoutManager linearLayoutManager = new LinearLayoutManager(getContext());
        if (recyclerView != null) {
            recyclerView.setLayoutManager(linearLayoutManager);
            recyclerView.addOnScrollListener(new EndlessRecyclerOnScrollListener(linearLayoutManager) {
                @Override
                public void onLoadMore(int current_page) {

                }
            });
            recyclerView.setHasFixedSize(true);
            recyclerView.addItemDecoration(new DividerItemDecoration(getContext(), LinearLayoutManager.VERTICAL));
            recyclerView.setAdapter(listAdapter);
        }
        return rootView;
    }
   @Override
    public void onStop() {
        super.onStop();
        recyclerView.clearOnScrollListeners();
    }
    @Override
    public void getMediaFinish(List<Media> medias) {
        for (Media media : medias) {
            mediaList.add(media);
        }
        listAdapter.notifyDataSetChanged();
    }
}
