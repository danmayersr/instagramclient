package com.mayer.instagramclient.model;

import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;
import android.database.sqlite.SQLiteStatement;

import com.mayer.instagramclient.AndroidUtilities;

import java.util.Date;

/* StateManager used to persist user state to persistent storage using SQLite store
*  Implementation by two tables: configuration tuples and request/response cache.
* */
public class StateManager {
    // Names of known app config tuple keys
    public static class Codes
    {
        public static final String AccessToken="AccessToken";
        public static final String Id="Id";
        public static final String UserName="UserName";
        public static final String Name="Name";
        public static final String ProfilePicture="ProfilePicture";
        public static final String Description="Description";
        public static final String SelfMediaRecent="SelfMediaRecent";
   }
    public static StateManager newInstance(Context context){return new StateManager(context);}
    private static final String DATABASE_NAME = "app.db";
    private static final int DATABASE_VERSION = 1;
    private Context context;
    private SQLiteDatabase db;
    private OpenHelper oh ;
    public StateManager(Context context) {
        this.context = context;
        this.oh = new OpenHelper(this.context);
        this.db = oh.getWritableDatabase();
    }
    public void close()
    {
        db.close();
        oh.close();
        db = null;
        oh = null;
        SQLiteDatabase.releaseMemory();
    }
    public void setCode(String codeName, Object codeValue, String codeDataType)
    {
        Cursor codeRow = db.rawQuery("SELECT * FROM code WHERE codeName = '"+  codeName + "'", null);
        String cv = "" ;
        if (codeDataType.toLowerCase().trim().equals("long") == true)
        {
            cv = String.valueOf(codeValue);
        }
        else if (codeDataType.toLowerCase().trim().equals("int") == true)
        {
            cv = String.valueOf(codeValue);
        }
        else if (codeDataType.toLowerCase().trim().equals("date") == true)
        {
            cv = String.valueOf(((Date)codeValue).getTime());
        }
        else if (codeDataType.toLowerCase().trim().equals("boolean") == true)
        {
            String.valueOf(codeValue);
        }
        else// default 'string'
        {
            cv = String.valueOf(codeValue);
        }
        if(codeRow.getCount() > 0) //exists-- update
        {
            db.execSQL("update code set codeValue = '" + cv + "' where codeName = '" + codeName + "'");
        }
        else // does not exist, insert
        {
            db.execSQL("INSERT INTO code (codeName, codeValue, codeDataType) VALUES(" +
                    "'" + codeName + "'," +
                    "'" + cv + "'," +
                    "'" + codeDataType + "')" );
        }
    }
    public Object getCode(String codeName, Object defaultValue)
    {
        //Check to see if it already exists
        String codeValue = "";
        String codeDataType = "";
        boolean found = false;
        Cursor codeRow  = db.rawQuery("SELECT * FROM code WHERE codeName = '"+  codeName + "'", null);
        if (codeRow.moveToFirst())
        {
            codeValue = codeRow.getString(codeRow.getColumnIndex("codeValue"));
            codeDataType = codeRow.getString(codeRow.getColumnIndex("codeDataType"));
            found = true;
        }

        if (found == false)
        {
            return defaultValue;
        }
        else if (codeDataType.toLowerCase().trim().equals("long") == true)
        {
            if (codeValue.equals("") == true)
            {
                return (long)0;
            }
            return Long.parseLong(codeValue);
        }
        else if (codeDataType.toLowerCase().trim().equals("int") == true)
        {
            if (codeValue.equals("") == true)
            {
                return (int)0;
            }
            return Integer.parseInt(codeValue);
        }
        else if (codeDataType.toLowerCase().trim().equals("date") == true)
        {
            if (codeValue.equals("") == true)
            {
                return null;
            }
            return new Date(Long.parseLong(codeValue));
        }
        else if (codeDataType.toLowerCase().trim().equals("boolean") == true)
        {
            if (codeValue.equals("") == true)
            {
                return false;
            }
            return Boolean.parseBoolean(codeValue);
        }
        else
        {
            return (String)codeValue;
        }
    }
    public void setJson(String request, byte[] response)
    {
        request= AndroidUtilities.getSha1Hex(request);
        if(response==null)
            response=new byte[0];
        Cursor row = db.rawQuery("SELECT * FROM json WHERE request = '"+  request + "'", null);
        if(row.getCount() > 0) //exists-- update
        {
            SQLiteStatement p = db.compileStatement("update json set response=?, responseTime=?");
            p.bindBlob(1, response);
            p.bindLong(2,System.currentTimeMillis());
            p.execute();
        }
        else // does not exist, insert
        {
            SQLiteStatement p = db.compileStatement("insert into json(request, response, requestTime, responseTime) VALUES(?,?,?, ?)");
            p.bindLong(4,System.currentTimeMillis());
            p.bindLong(3,System.currentTimeMillis());
            p.bindBlob(2, response);
            p.bindString(1,request);
            p.execute();
        }
    }
    //notify state that a request has occurred
    public void setJsonRequest(String request){
        request= AndroidUtilities.getSha1Hex(request);
        Cursor row = db.rawQuery("SELECT * FROM json WHERE request = '"+  request + "'", null);
        if(row.getCount() > 0) //exists-- update
        {
            SQLiteStatement p = db.compileStatement("update json set requestTime=? where request='?'");
            p.bindString(2, request);
            p.bindLong(1,System.currentTimeMillis());
            p.execute();
        }
        else // does not exist, insert
        {
            SQLiteStatement p = db.compileStatement("insert into json(request, response, requestTime, responseTime) VALUES(?,?,?, ?)");
            p.bindLong(4,0);
            p.bindLong(3,System.currentTimeMillis());
            p.bindBlob(2, null);
            p.bindString(1,request);
            p.execute();
        }
    }
    public Boolean getIsResponseRecent(String request){
        request= AndroidUtilities.getSha1Hex(request);
        Long responseTime;
        Cursor row  = db.rawQuery("SELECT requestTime FROM json WHERE request = '"+  request + "'", null);
        if (row.moveToFirst())
        {
            responseTime = row.getLong(row.getColumnIndex("requestTime"));
            if(responseTime==0)return false;
            long totalMillis = System.currentTimeMillis() - responseTime;
            int seconds = (int) (totalMillis / 1000) % 60;
            return seconds<20;
        }
        return false;
    }
    // determine if cache response is old (less than an hour)
    public Boolean getIsResponseOld(String request){
        request= AndroidUtilities.getSha1Hex(request);
        Long responseTime;
        Cursor row  = db.rawQuery("SELECT responseTime FROM json WHERE request = '"+  request + "'", null);
        if (row.moveToFirst())
        {
            responseTime = row.getLong(row.getColumnIndex("responseTime"));
            long totalMillis = System.currentTimeMillis() - responseTime;
            int hours = (int)(totalMillis / 1000) / 3600;
            return hours>1;
        }
        return false;
    }
    public byte[] getJson(String request)
    {
        request= AndroidUtilities.getSha1Hex(request);
        Cursor row  = db.rawQuery("SELECT response FROM json WHERE request = '"+  request + "'", null);
        if (row.moveToFirst())
        {
            return row.getBlob(row.getColumnIndex("response"));
        }
        return null;
    }
    // return response if it is outdated
    public byte[] getJsonCache(String request)
    {
        request= AndroidUtilities.getSha1Hex(request);
        Cursor row  = db.rawQuery("SELECT response, responseTime FROM json WHERE request = '"+  request + "'", null);
        if (row.moveToFirst())
        {
            byte[] response = row.getBlob(row.getColumnIndex("response"));
            long responseTime = row.getLong(row.getColumnIndex("responseTime"));
            long totalMillis = System.currentTimeMillis() - responseTime;
            int hours = (int)(totalMillis / 1000) / 3600;
            if(hours<1)
                return response;
            else
                return null;//outdated
        }
        return null;
    }
    public void reset(){
        db.execSQL("DROP TABLE code ");
        db.execSQL("DROP TABLE json ");
        close();
        this.oh = new OpenHelper(this.context);
        this.db = oh.getWritableDatabase();
    }
    private static class OpenHelper extends SQLiteOpenHelper {
        OpenHelper(Context context) {
            super(context, DATABASE_NAME, null, DATABASE_VERSION);
        }
        @Override
        public void onCreate(SQLiteDatabase db) {
            db.execSQL("CREATE TABLE IF NOT EXISTS code " +
                    "(id INTEGER PRIMARY KEY, codeName TEXT, codeValue TEXT, codeDataType TEXT)");
            db.execSQL("CREATE TABLE IF NOT EXISTS json " +
                    "(id INTEGER PRIMARY KEY, request TEXT, response BLOB, requestTime INTEGER, responseTime INTEGER)");
        }
        @Override
        public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
        }
    }
}